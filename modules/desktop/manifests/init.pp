class desktop {

	file { '/etc/skel/Desktop':
		ensure => directory,
	}

	package { 'xinit':
		ensure => installed,
	}

	package { 'lxde':
		ensure => installed,
		require => Package['xinit'],
	}

	$dms = [
		'lightdm*',
		'kdm',
		'gdm3',
		'nodm',
		'wdm',
		'xdm',
		'slim',
	]

	package { $dms:
		ensure => absent,
		require => Package['lxde'],
	}

	package { 'xscreensaver':
		ensure => absent,
	}

	$packages = [
		'pcmanfm',
		'firefox-esr',
		'firefox-esr-l10n-*',
		'gstreamer1.0-libav',
		'gstreamer1.0-plugins-good',
		'flashplugin-nonfree',
		'chromium',
		'chromium-l10n',
		'libreoffice',
#		'libreoffice-help-*',
#		'libreoffice-l10n-*',
		'ttf-mscorefonts-installer',
		'ttf-liberation',
		'fonts-crosextra-carlito',
		'fonts-crosextra-caladea',
                'florence',
		'vlc',
		'browser-plugin-vlc',
		'youtube-dl',
	]
  
	package { $packages:
		ensure => installed,
	}

	file { '/etc/skel/Desktop/libreoffice-startcenter.desktop':
		source => '/usr/share/applications/libreoffice-startcenter.desktop',
		require => [
			Package['libreoffice'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/skel/Desktop/lxde-logout.desktop':
		source => '/usr/share/applications/lxde-logout.desktop',
		require => [
			Package['lxde'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/skel/Desktop/leafpad.desktop':
		source => '/usr/share/applications/leafpad.desktop',
		replace => false,
		require => [
			Package['lxde'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'fix-desktop-leafpad':
		command => '/bin/sed -i "/^Name/ s/=.*/=Notes/;" /etc/skel/Desktop/leafpad.desktop',
		unless => '/bin/grep -q "^Name=Notes" /etc/skel/Desktop/leafpad.desktop',
		require => File['/etc/skel/Desktop/leafpad.desktop'],
	}

	file { '/etc/skel/Desktop/galculator.desktop':
		source => '/usr/share/applications/galculator.desktop',
		replace => false,
		require => [
			Package['lxde'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'fix-desktop-galculator':
		command => '/bin/sed -i "/^Name/ s/=.*/=Calculator/;" /etc/skel/Desktop/galculator.desktop',
		unless => '/bin/grep -q "^Name.*=Calculator" /etc/skel/Desktop/galculator.desktop',
		require => File['/etc/skel/Desktop/galculator.desktop'],
	}

	file { '/etc/skel/Desktop/chromium-l10n.desktop':
		source => '/usr/share/applications/chromium.desktop',
		require => [
			Package['chromium'],
			File['/etc/skel/Desktop'],
		],
	}
	
	file { '/etc/skel/Desktop/florence.desktop':
		source => '/usr/share/applications/florence.desktop',
		replace => false,
		require => [
			Package['florence'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'fix-desktop-florence':
		command => '/bin/sed -i "/^Name/ s/=.*/=Keyboard/;" /etc/skel/Desktop/florence.desktop',
		unless => '/bin/grep -q "^Name.*=Keyboard" /etc/skel/Desktop/florence.desktop',
		require => File['/etc/skel/Desktop/florence.desktop'],
	}

	file { '/etc/skel/Desktop/pcmanfm.desktop':
		source => '/usr/share/applications/pcmanfm.desktop',
		replace => false,
		require => [
			Package['pcmanfm'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'fix-desktop-pcmanfm':
		command => '/bin/sed -i "/^Name/ s/=.*/=Filemanager/;" /etc/skel/Desktop/pcmanfm.desktop',
		unless => '/bin/grep -q "^Name.*=Filemanager" /etc/skel/Desktop/pcmanfm.desktop',
		require => File['/etc/skel/Desktop/pcmanfm.desktop'],
	}

	file { '/etc/skel/Desktop/iceweasel.desktop':
		ensure => absent,
	}

	file { '/etc/skel/Desktop/firefox-esr.desktop':
		source => '/usr/share/applications/firefox-esr.desktop',
		replace => false,
		require => [
			Package['firefox-esr'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'fix-desktop-firefox-esr':
		command => '/bin/sed -i "/^Name/ s/=.*/=Webbrowser/;" /etc/skel/Desktop/firefox-esr.desktop',
		unless => '/bin/grep -q "^Name.*=Webbrowser" /etc/skel/Desktop/firefox-esr.desktop',
		require => File['/etc/skel/Desktop/firefox-esr.desktop'],
	}

	file { '/etc/firefox-esr/prefs.js':
		source => 'puppet:///modules/desktop/etc/firefox-esr/prefs.js',
		require => [
			Package['firefox-esr'],
		],
	}

	group { 'guest':
		ensure => 'present'
	}

	user { 'guest':
		uid => 5000,
		home => '/run/user/5000/home',
		# password: guest
		password => '$6$pheeghohphaikuka$lJodCaTfZG4IhXw9.htxDsMDAiQrhOsXyJR.nvCdWc2EHgmstkTrpn/8LWi83e9Ac.WFDfzmQ1phpDII/npl/0',
		managehome => false,
		gid => 'guest',
		groups => [
				'plugdev',
				'cdrom',
				'audio',
				'video',
			],
		shell => '/bin/bash',
		require => Group['guest'],
		ensure => 'present',
	}

	$packages_i386 = [
		'libc6:i386',
		'libqt4-dbus:i386',
		'libqt4-network:i386',
		'libqt4-xml:i386',
		'libqtcore4:i386',
		'libqtgui4:i386',
		'libqtwebkit4:i386',
		'libstdc++6:i386',
		'libx11-6:i386',
		'libxext6:i386',
		'libxss1:i386',
		'libxv1:i386',
		'libssl1.0.0:i386',
		'libpulse0:i386',
		'libasound2-plugins:i386',
	]

	package { $packages_i386:
		ensure => installed,
		require => [
				Exec['add-architecture-i386'],
				Exec['apt_update'],
			],
	}

	exec { 'download-skype':
		command => '/usr/bin/wget -qO/var/cache/.skype-install.deb.part http://www.skype.com/go/getskype-linux-deb && /bin/mv /var/cache/.skype-install.deb.part /var/cache/skype-install.deb',
		creates => '/var/cache/skype-install.deb',
	}

	package { 'skype':
		provider => dpkg,
		ensure => latest,
		source => '/var/cache/skype-install.deb',
		require => [
			Exec['download-skype'],
			Package['libc6:i386'],
			],	
	}

	file { '/etc/skel/Desktop/skype.desktop':
		source => '/usr/share/applications/skype.desktop',
		require => [
			Package['skype'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'enable-guestx11':
		command => '/bin/systemctl enable guestx11.service',
		refreshonly => true,
	}
		
	exec { 'start-guestx11':
		command => '/bin/systemctl start guestx11.service',
		refreshonly => true,
	}

	file { '/usr/local/sbin/x11login':
		source => 'puppet:///modules/desktop/usr/local/sbin/x11login',
		mode => 'a=rx',
		ensure => file,
	}

	file { '/etc/skel':
		source => 'puppet:///modules/desktop/etc/skel',
		recurse => true,
	}

	file { '/etc/systemd/system/guestx11.service':
		source => 'puppet:///modules/desktop/etc/systemd/system/guestx11.service',
		mode => 'a=rx',
		require => [
				Package['lxde'],
				File['/etc/skel/Desktop'],
				File['/usr/local/sbin/x11login'],
				File['/etc/skel'],
			],
		notify => [
				Exec['enable-guestx11'],
				Exec['start-guestx11'],
			],
		ensure => file,
	}
	
}
