class persist {

	file { '/usr/local/sbin/git2puppet':
		ensure => present,
		source => 'puppet:///modules/persist/usr/local/sbin/git2puppet',
		mode => '0755',
	}

	$packages = [
		'git-core',
		'puppet',
	]
  
	package { $packages:
		ensure => installed,
	}	

	service { 'puppet':
		enable => false,
	}

}
